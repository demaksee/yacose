# Yacose

Async command service - Android library that let you executing tasks in service

#### Gradle

Add the following dependency to your build.gradle file for your project:

    dependencies {
      compile 'com.demaksee:yacose:0.9.4'
    }

Add executor service into your project by adding following to your AndroidManifest.xml

    <service
            android:name="com.demaksee.yacose.service.CommandExecutorService"
            android:exported="false"/>

Or you can extend CommandExecutorService, override configuration of thread pool executor and publish this descendant it the manifest

# Usage

Extend AbstractCommand and implement run method - it will be performed in service in thread executor in service, and their won't depend on Activity/Fragment lifecycle.

Extend BaseServiceHelper and add methods to start your tasks.

Implement OnServiceResultListener and register/unregister it in onResume()/onPause() methods correspondingly - here you will get your result. But don't forget to check execution state in onResume.

# Samples

To find sample of usage please browse the [samples](https://bitbucket.org/demaksee/yacose/src/master/app/src/main/java/com/demaksee/yacose/example/?at=master)

# License

     Copyright 2015 demaksee
     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
