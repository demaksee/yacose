/*
 * Copyright 2015 demaksee
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demaksee.yacose;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.demaksee.yacose.handler.AbstractCommand;
import com.demaksee.yacose.request.Request;
import com.demaksee.yacose.result.Result;
import com.demaksee.yacose.service.CommandExecutorService;

public class BaseServiceHelper {

	public static final int DEFAULT_REQUEST_ID = 0;
	
	public static final int REQUEST_ID_MIN_VALUE = 1000;

	private final AtomicInteger idCounter = new AtomicInteger(REQUEST_ID_MIN_VALUE);

	private final Handler handler;

	private final CopyOnWriteArrayList<OnServiceResultListener> currentListeners = new CopyOnWriteArrayList<OnServiceResultListener>();

	private final ConcurrentHashMap<Integer, Intent> pendingRequests = new ConcurrentHashMap<Integer, Intent>();

	private final Context context;

	private final Class<? extends CommandExecutorService> serviceClass;

	public interface OnServiceResultListener {
	    void onServiceResult(Request request, int resultCode, Result result);
	}
	
	public BaseServiceHelper(Context context, Handler handler) {
		this.context = context;
		this.handler = handler;
		this.serviceClass = CommandExecutorService.class;
	}

	public BaseServiceHelper(Context context, Handler handler, Class<? extends CommandExecutorService> serviceClass) {
		this.context = context;
		this.handler = handler;
		this.serviceClass = serviceClass;
	}

	public void addListener(OnServiceResultListener currentListener) {
		currentListeners.add(currentListener);
	}

	public void removeListener(OnServiceResultListener currentListener) {
		currentListeners.remove(currentListener);
	}

	public boolean isPending(int requestId) {
		return pendingRequests.containsKey(requestId);
	}

	protected int createId() {
		return idCounter.getAndIncrement();
	}

	protected int runRequest(final int requestId, Intent i) {
		pendingRequests.put(requestId, i);
		context.startService(i);
		return requestId;
	}

	/**
	 *
	 * @param requestId
	 * @param intent
	 * @param cancelPriorPendingRequests true means cancel already enqueued and ongoing tasks
	 * @return
	 */
	protected int runRequestInSeries(final int requestId, Intent intent, boolean cancelPriorPendingRequests) {
		intent.putExtra(AbstractCommand.EXTRA_SERIAL_COMMAND, true);
		intent.putExtra(CommandExecutorService.EXTRA_CANCEL_PRIOR_PENDING_REQUESTS, cancelPriorPendingRequests);
		return runRequest(requestId, intent);
	}

	protected Intent createIntent(Class<? extends AbstractCommand> cls, final int requestId, Parcelable clientData) {
		Intent intent = new Intent(context, serviceClass);
		intent.setAction(cls.getName());
		intent.putExtra(AbstractCommand.EXTRA_CLIENT_DATA, clientData);
		intent.putExtra(AbstractCommand.EXTRA_REQUEST_ID, requestId);
		intent.putExtra(AbstractCommand.EXTRA_RESULT_RECEIVER,
				new ResultReceiver(handler) {
					@Override
					protected void onReceiveResult(int resultCode, Bundle resultData) {
						Intent originalIntent = resultCode != AbstractCommand.RESULT_PROGRESS ?
								pendingRequests.remove(requestId) : pendingRequests.get(requestId);
						Parcelable resultClientData = originalIntent.getParcelableExtra(AbstractCommand.EXTRA_CLIENT_DATA);
						Request request = new Request(requestId, originalIntent.getAction(), resultClientData, originalIntent);
						Result result = null;
						if (resultData != null) {
							resultData.setClassLoader(context.getClassLoader());
							result = resultData.getParcelable(AbstractCommand.EXTRA_RESULT);
						}
						if (result == null) {
							result = new Result();
						}
						result.rawResultBundle = resultData;
						for (OnServiceResultListener currentListener : currentListeners) {
							currentListener.onServiceResult(request, resultCode, result);
						}
					}
				});

		return intent;
	}

	private Intent prepareCancelIntent(int requestId) {
		Intent cancelIntent = new Intent(context, serviceClass);
		cancelIntent.setAction(CommandExecutorService.ACTION_CANCEL);
		cancelIntent.putExtra(AbstractCommand.EXTRA_REQUEST_ID, requestId);
		return cancelIntent;
	}
	
	/**
	 * <p>Send cancel signal to particular task.
	 * Cancellation should be considered in implementation particular task.
	 * See {@link AbstractCommand#checkCanceled()}
	 *
	 * @param requestId of task that should be canceled
	 * @return true if command was pending in moment of canceling
	 */
	public boolean cancelRequest(int requestId){
		if (requestId == DEFAULT_REQUEST_ID || !isPending(requestId)){
			return false;
		}
			
		context.startService(prepareCancelIntent(requestId));
		return true;
	}
}
