/*
 * Copyright 2015 demaksee
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demaksee.yacose.result;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.demaksee.yacose.handler.AbstractCommand;

/**
 * Contains result of particular task.
 * @param <T> - type of your structure
 */
public class Result<T extends Parcelable> implements Parcelable {

	/**
	 * Contains data, put in clientData object during task invocation.
	 * See {@link AbstractCommand#getClientData()}
	 */
	public T clientData;

	/**
	 * Contains errorMessage.
	 * See {@link AbstractCommand#postFailure(String)}
	 */
	public String errorMessage;

	/**
	 * Contains progress if current task is sent to notify about execution progress.
	 * See {@link AbstractCommand#postProgress(String, int, int)}
	 */
	public Progress progress;

	public transient Bundle rawResultBundle;

	public Result() {
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.clientData, 0);
		dest.writeString(this.errorMessage);
		dest.writeParcelable(this.progress, 0);
	}

	private Result(Parcel in) {
		this.clientData = in.readParcelable(Parcelable.class.getClassLoader());
		this.errorMessage = in.readString();
		this.progress = in.readParcelable(Progress.class.getClassLoader());
	}

	public static final Creator<Result> CREATOR = new Creator<Result>() {
		public Result createFromParcel(Parcel source) {
			return new Result(source);
		}

		public Result[] newArray(int size) {
			return new Result[size];
		}
	};
}
