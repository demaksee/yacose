/*
 * Copyright 2015 demaksee
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demaksee.yacose.result;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Contains progress of particular task
 */
public class Progress implements Parcelable {

	public String progressMessage;

	public int currentProgress;

	public int totalProgress;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.progressMessage);
		dest.writeInt(this.currentProgress);
		dest.writeInt(this.totalProgress);
	}

	public Progress() {
	}

	private Progress(Parcel in) {
		this.progressMessage = in.readString();
		this.currentProgress = in.readInt();
		this.totalProgress = in.readInt();
	}

	public static final Parcelable.Creator<Progress> CREATOR = new Parcelable.Creator<Progress>() {
		public Progress createFromParcel(Parcel source) {
			return new Progress(source);
		}

		public Progress[] newArray(int size) {
			return new Progress[size];
		}
	};
}
