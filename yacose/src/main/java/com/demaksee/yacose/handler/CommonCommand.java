package com.demaksee.yacose.handler;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;

import com.demaksee.yacose.Yacose;
import com.demaksee.yacose.exception.CanceledTaskException;
import com.demaksee.yacose.exception.FailedTaskException;

/**
 * Created by demaksee on 17.01.16.
 */
public abstract class CommonCommand<T extends Parcelable> extends AbstractCommand<T> {

    private static final String TAG = CommonCommand.class.getSimpleName();

    public CommonCommand(Context context, Intent intent) {
        super(context, intent);
    }

    @Override
    public void run() {
        try {
            onRun();

            postSuccess();
        } catch (CanceledTaskException e) {
            postCanceled();
            if (Yacose.isLoggingEnabled()) {
                Log.d(TAG, "Task canceled | requestId " + getRequestId(), e);
            }
        } catch (FailedTaskException e) {
            postFailure(e.getMessage());
            if (Yacose.isLoggingEnabled()) {
                Log.d(TAG, "Task failed | requestId " + getRequestId(), e);
            }
        }
    }

    public abstract void onRun() throws CanceledTaskException, FailedTaskException;
}
