/*
 * Copyright 2015 demaksee
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demaksee.yacose.handler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Log;

import com.demaksee.yacose.BaseServiceHelper;
import com.demaksee.yacose.Yacose;
import com.demaksee.yacose.exception.CanceledTaskException;
import com.demaksee.yacose.result.Progress;
import com.demaksee.yacose.result.Result;

public abstract class AbstractCommand<T extends Parcelable> implements Runnable{

	private static final String TAG = AbstractCommand.class.getSimpleName();

	public static final int RESULT_PROGRESS = 1;

    public static final int RESULT_SUCCESS = 0;

    public static final int RESULT_FAILURE = -1;

    public static final int RESULT_CANCELED = -2;

	public static final String EXTRA_REQUEST_ID = "com.demaksee.yacose.REQUEST_ID";

	public static final String EXTRA_CLIENT_DATA = "com.demaksee.yacose.CLIENT_DATA";

	public static final String EXTRA_SERIAL_COMMAND = "com.demaksee.yacose.SERIAL_COMMAND";

	public static final String EXTRA_RESULT = "com.demaksee.yacose.RESULT";

    public static final String EXTRA_RESULT_RECEIVER = "com.demaksee.yacose.RESULT_RECEIVER";

	private Context context;

	private ResultReceiver resultReceiver;

	private final int requestId;

	private boolean isResultPosted = false;

	private final T clientData;

	private Bundle progressBundle;

	private Result progressResult;

	private final boolean serial;

	private final Result<T> result = new Result<>();

	public AbstractCommand(Context context, Intent intent) {
		this.context = context;
		resultReceiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
		requestId = intent.getIntExtra(EXTRA_REQUEST_ID, BaseServiceHelper.DEFAULT_REQUEST_ID);

		if (intent.hasExtra(EXTRA_CLIENT_DATA)) {
			clientData = intent.getParcelableExtra(EXTRA_CLIENT_DATA);
		} else {
			clientData = null;
		}

		serial = intent.getBooleanExtra(EXTRA_SERIAL_COMMAND, false);
	}

	public final T getClientData() {
		return clientData;
	}

	/**
	 * Post {@link Result#progress} to listeners
	 * @param progressMessage - optional
	 * @param currentProgress - current progress
	 * @param totalProgress - total progress
	 */
	public final void postProgress(String progressMessage, int currentProgress, int totalProgress) {
		if (progressBundle == null) {
			progressBundle = new Bundle();
			progressResult = new Result();
			progressResult.progress = new Progress();
			progressBundle.putParcelable(EXTRA_RESULT, progressResult);
		}
		progressResult.progress.progressMessage = progressMessage;
		progressResult.progress.currentProgress = currentProgress;
		progressResult.progress.totalProgress = totalProgress;
		postProgressResult();
	}

	private void postProgressResult() {
		progressResult.clientData = clientData;

		postRawResult(RESULT_PROGRESS, progressBundle);
	}

	public final void postFailure(String errorMessage) {
		result.errorMessage = errorMessage;
		postResult(RESULT_FAILURE);
	}

	public final void postSuccess() {
		postResult(RESULT_SUCCESS);
	}

	public final void postCanceled() {
		postResult(RESULT_CANCELED);
	}

	private void postResult(int resultCode) {
		result.clientData = cloneParcelable(clientData);
		Bundle bundle = new Bundle();
		bundle.putParcelable(EXTRA_RESULT, result);

		postRawResult(resultCode, bundle);
	}

	/**
	 * Allows to assembly and send custom result bundle.
	 * @param resultCode - {@link #RESULT_SUCCESS}, {@link #RESULT_FAILURE}, etc
	 * @param resultData - your bundle with custom data
	 */
	public final void postRawResult(int resultCode, Bundle resultData){
		if (Yacose.isLoggingEnabled()) {
			String msg = "posting " + toString(resultCode) + " for requestCode " + getRequestId();
			if (resultData != null ) {
				msg += " with bundle " + resultData.toString();
			}
			Log.d(TAG,  msg);
		}
		if (isResultPosted && resultCode != RESULT_PROGRESS) {
			throw new RuntimeException("Result has already been posted");
		}
		resultReceiver.send(resultCode, resultData);
    	if (resultCode != RESULT_PROGRESS){
    		isResultPosted = true;
    	}
    }
    
    public boolean isResultPosted() {
		return isResultPosted;
	}
    
    public Context getContext() {
		return context;
	}
    
    public int getRequestId() {
		return requestId;
	}

	public boolean isSerial() {
		return serial;
	}

	/**
	 *
	 * @return return true if current task is canceled by user
	 */
    protected final boolean isCanceled(){
    	return Thread.currentThread().isInterrupted();
    }

	/**
	 *
	 * @throws CanceledTaskException if current task is canceled by user
	 */
	protected final void checkCanceled() throws CanceledTaskException {
		if (isCanceled()) {
			throw new CanceledTaskException("Canceled");
		}
	}

	private T cloneParcelable(T obj) {
		if (obj == null) {
			return null;
		}
		Parcel parcel = Parcel.obtain();
		try {
			parcel.writeValue(obj);
			parcel.setDataPosition(0);
			@SuppressWarnings("unchecked")
			T clone = (T) parcel.readValue(obj.getClass().getClassLoader());
			return clone;
		} finally {
			parcel.recycle();
		}
	}

    public static String toString(int result) {
    	switch (result) {
		case RESULT_PROGRESS:
			return "RESULT_PROGRESS";
		case RESULT_SUCCESS:
			return "RESULT_SUCCESS";
		case RESULT_FAILURE:
			return "RESULT_FAILURE";
		case RESULT_CANCELED:
			return "RESULT_CANCELED";
		default:
			return "RES_UNKNOWN";
		}
    }
}
