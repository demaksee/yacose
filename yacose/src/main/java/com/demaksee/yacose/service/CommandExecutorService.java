/*
 * Copyright 2015 demaksee
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demaksee.yacose.service;

import java.lang.reflect.Constructor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;
import android.util.SparseArray;

import com.demaksee.yacose.Yacose;
import com.demaksee.yacose.handler.AbstractCommand;

public class CommandExecutorService extends Service {

	private static final String TAG = CommandExecutorService.class.getSimpleName();

	public static final String ACTION_CANCEL = "com.demaksee.yacose.action.CANCEL";

	public static final String EXTRA_CANCEL_PRIOR_PENDING_REQUESTS = "com.demaksee.yacose.CANCEL_PRIOR_PENDING_REQUESTS";

	private ExecutorService executor;
	
	private SparseArray<CommandInfo> pendingCommands = new SparseArray<CommandInfo>();
	
	private static final int SHUTDOWN_DELAY = 1000 * 60;
	
	private Handler handler = new Handler();
	
	private int startId;

	private AntiSemaphore antiSemaphore = new AntiSemaphore();

	private Runnable shutdownRunnable = new Runnable() {

		@Override
		public void run() {
			stopSelf(startId);
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();
		executor = onCreateExecutor(getShutdownDelay());
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		executor.shutdown();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		this.startId = startId;
		if (ACTION_CANCEL.equals(intent.getAction())){
			cancelCommand(intent);
		} else {
			enqueueCommand(intent);
		}
		return START_NOT_STICKY;
	}

	private void enqueueCommand(Intent intent) {
		AbstractCommand cmd = createCommand(intent);
		if (cmd != null) {
			synchronized (CommandExecutorService.this) {
				if (intent.getBooleanExtra(EXTRA_CANCEL_PRIOR_PENDING_REQUESTS, false)) {
					int i = 0;
					while (i < pendingCommands.size()) {
						CommandInfo info = pendingCommands.valueAt(i);
						if (info.future.cancel(true) && !info.inProgress) {
							info.cmd.postCanceled();
							pendingCommands.remove(pendingCommands.keyAt(i));
						} else {
							i++;
						}
					}
				}
				Future<?> future = executor.submit(createHandler(cmd, intent.getAction()));
				pendingCommands.put(cmd.getRequestId(), new CommandInfo(future, cmd));
				handler.removeCallbacks(shutdownRunnable);
			}
		}
	}
	
	private void cancelCommand(Intent intent){
		int requestId = intent.getIntExtra(AbstractCommand.EXTRA_REQUEST_ID, 0);
        // FIXME: handle situation when cancel arrives earlier than corresponding task
		synchronized (CommandExecutorService.this) {
			CommandInfo info = pendingCommands.get(requestId);
			if (info != null){
				if (info.future.cancel(true) && !info.inProgress){
					info.cmd.postCanceled();
					pendingCommands.remove(requestId);
					if (pendingCommands.size() == 0){
						handler.postDelayed(shutdownRunnable, getShutdownDelay());
					}
				}
			}
		}
	}
	
	private AbstractCommand createCommand(Intent intent){
		try {
			@SuppressWarnings("unchecked") Class<? extends AbstractCommand> cls = (Class<? extends AbstractCommand>) Class.forName(intent.getAction());
			Constructor<? extends AbstractCommand> constructor = cls.getConstructor(Context.class, Intent.class);
			return constructor.newInstance(this, intent);
		} catch (Throwable t){
            if (Yacose.isLoggingEnabled()) {
                Log.e(TAG, "Cannot create command", t);
            }
		}
		return null;
	}
	
	private Runnable createHandler(final AbstractCommand cmd, final String action) {
		return new Runnable() {
			
			@Override
			public void run() {
				synchronized (CommandExecutorService.this) {
                    if (Thread.currentThread().isInterrupted()) {
                        // in this case postCanceled was invoked by cancelCommand(Intent) method,
                        // so just break invocation
                        return;
                    }
					pendingCommands.get(cmd.getRequestId()).inProgress = true;
				}
				boolean permitAcquired = false;
				try {
					antiSemaphore.acquire(cmd.isSerial());
					permitAcquired = true;
					Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
					cmd.run();
				} catch (InterruptedException e) {
					cmd.postCanceled();
				} catch (Throwable e){
					if (Yacose.isLoggingEnabled()) {
						Log.d(TAG, "Command invocation error", e);
					}
				} finally{
					if (!cmd.isResultPosted()){
						cmd.postFailure(null);
                        if (Yacose.isLoggingEnabled()) {
                            Log.w(TAG, "Command " + action + " hasn't posted up any result, "
                                    + "RESULT_FAILURE has been posted manually.");
                        }
					}
					synchronized (CommandExecutorService.this) {
						pendingCommands.remove(cmd.getRequestId());
						if (pendingCommands.size() == 0){
							handler.postDelayed(shutdownRunnable, getShutdownDelay());
						}
					}
					if (permitAcquired) {
						antiSemaphore.release(cmd.isSerial());
					}
				}
			}
		};
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	protected ExecutorService onCreateExecutor(int shutdownDelay) {
		return new ThreadPoolExecutor(6, 16, shutdownDelay,
				TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	}

	/**
	 * Override this method if you want to set another delay for keeping alive idle executor and service.
	 * Also this value will be used as keepAliveTime in default implementation of {@link #onCreateExecutor(int)}
	 * @return shutdown delay in millis, after which idle executor and service will stop
	 */
	protected int getShutdownDelay() {
		return SHUTDOWN_DELAY;
	}

	private static class CommandInfo {

		public final Future<?> future;
		
		public final AbstractCommand cmd;
		
		public boolean inProgress = false;
		
		public CommandInfo(Future<?> future, AbstractCommand cmd) {
			this.future = future;
			this.cmd = cmd;
		}
	}

	private static class AntiSemaphore {

		private static final int SIZE = Integer.MAX_VALUE;

		private Semaphore semaphore = new Semaphore(SIZE, true);

		public void acquire(boolean serial) throws InterruptedException {
			if (serial) {
				semaphore.acquire(SIZE);
			} else {
				semaphore.acquire();
			}
		}

		public void release(boolean serial) {
			if (serial) {
				semaphore.release(SIZE);
			} else {
				semaphore.release();
			}
		}
	}
}
