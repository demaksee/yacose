/*
 * Copyright 2015 demaksee
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demaksee.yacose.exception;

/**
 * Intended for breaking task invocation
 * 
 */
public class CanceledTaskException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CanceledTaskException() {
	}

	public CanceledTaskException(String detailMessage) {
		super(detailMessage);
	}

	public CanceledTaskException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public CanceledTaskException(Throwable throwable) {
		super(throwable);
	}
}
