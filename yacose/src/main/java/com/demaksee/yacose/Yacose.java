package com.demaksee.yacose;

/**
 * Created by demaksee on 18.01.16.
 */
public class Yacose {

    private static boolean loggingEnabled = false;

    public static void setLoggingEnabled(boolean enabled) {
        loggingEnabled = enabled;
    }

    public static boolean isLoggingEnabled() {
        return loggingEnabled;
    }
}
