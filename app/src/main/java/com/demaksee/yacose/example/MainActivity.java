package com.demaksee.yacose.example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.demaksee.yacose.BaseServiceHelper;
import com.demaksee.yacose.example.service.ServiceHelper;
import com.demaksee.yacose.example.service.command.SumCommand;
import com.demaksee.yacose.handler.AbstractCommand;
import com.demaksee.yacose.request.Request;
import com.demaksee.yacose.result.Result;

public class MainActivity extends Activity {

	public static final String SUM_REQUEST_ID = "sumRequestId";

	private ServiceHelper serviceHelper;
	private int sumRequestId = BaseServiceHelper.DEFAULT_REQUEST_ID;
	private ProgressBar progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (serviceHelper.isPending(sumRequestId)) {
					serviceHelper.cancelRequest(sumRequestId);
				} else {
					sumRequestId = serviceHelper.calculateSum(1, 2);
					progressBar.setVisibility(View.VISIBLE);
				}
			}
		});

		findViewById(R.id.buttonOpenSerialExample).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this, SerialExampleActivity.class));
			}
		});

		serviceHelper = YacoseExampleApplication.getInstance().getServiceHelper();

		if (savedInstanceState != null) {
			sumRequestId = savedInstanceState.getInt(SUM_REQUEST_ID);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		serviceHelper.addListener(onServiceResultListener);

		if (sumRequestId != BaseServiceHelper.DEFAULT_REQUEST_ID && !serviceHelper.isPending(sumRequestId)) {
			progressBar.setVisibility(View.INVISIBLE);
			// you can check result in any persistent storage here
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		serviceHelper.removeListener(onServiceResultListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(com.demaksee.yacose.example.R.menu.menu_main, menu);
		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putInt(SUM_REQUEST_ID, sumRequestId);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == com.demaksee.yacose.example.R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	BaseServiceHelper.OnServiceResultListener onServiceResultListener = new BaseServiceHelper.OnServiceResultListener() {
		@Override
		public void onServiceResult(Request request, int resultCode, Result result) {
			if (request.forCommand(SumCommand.class)) {
				if (resultCode == AbstractCommand.RESULT_SUCCESS) {
					progressBar.setVisibility(View.INVISIBLE);
					SumCommand.SumData sumData = (SumCommand.SumData) result.clientData;
					Toast.makeText(MainActivity.this, "Result: " + sumData.result, Toast.LENGTH_SHORT).show();
				} else if (resultCode == AbstractCommand.RESULT_FAILURE) {
					progressBar.setVisibility(View.INVISIBLE);
					Toast.makeText(MainActivity.this, "Something wrong: " + result.errorMessage, Toast.LENGTH_SHORT).show();
				} else if (resultCode == AbstractCommand.RESULT_PROGRESS) {
					Toast.makeText(MainActivity.this, "progress: " + result.progress.currentProgress + " / " + result.progress.totalProgress, Toast.LENGTH_SHORT).show();
				} else if (resultCode == AbstractCommand.RESULT_CANCELED) {
					progressBar.setVisibility(View.INVISIBLE);
					Toast.makeText(MainActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
				}
			}
		}
	};
}
