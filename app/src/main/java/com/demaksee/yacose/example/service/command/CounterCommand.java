package com.demaksee.yacose.example.service.command;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;

import com.demaksee.yacose.exception.CanceledTaskException;
import com.demaksee.yacose.exception.FailedTaskException;
import com.demaksee.yacose.handler.CommonCommand;

/**
 * Created by demaksee on 6/2/15.
 */
public class CounterCommand extends CommonCommand<CounterCommand.Data> {

	public CounterCommand(Context context, Intent intent) {
		super(context, intent);
	}

	@Override
	public void onRun() throws CanceledTaskException, FailedTaskException {
		try {

			int max = getClientData().max;
			for (int i = 0; i < max; i++) {
				SystemClock.sleep(100);
				postProgress(null, i, max);
				checkCanceled(); // check whether current task is canceled and throw exception if true
			}

		} catch (RuntimeException e){ // RuntimeException just for example
			throw new FailedTaskException(e.getMessage());
		}
	}

	/**
	 * Structure to hold parameters and results.
	 * You can use any plugin to generate parcelable boilerplate code:
	 * Android studio plugin: https://github.com/mcharmas/android-parcelable-intellij-plugin
	 * or something like: https://android-arsenal.com/details/1/1039
	 */
	public static class Data implements Parcelable {
		public int max;

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeInt(this.max);
		}

		public Data() {
		}

		protected Data(Parcel in) {
			this.max = in.readInt();
		}

		public static final Creator<Data> CREATOR = new Creator<Data>() {
			public Data createFromParcel(Parcel source) {
				return new Data(source);
			}

			public Data[] newArray(int size) {
				return new Data[size];
			}
		};
	}
}
