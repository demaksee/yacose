package com.demaksee.yacose.example;

import android.app.Application;
import android.os.Handler;

import com.demaksee.yacose.BuildConfig;
import com.demaksee.yacose.Yacose;
import com.demaksee.yacose.example.service.ServiceHelper;

/**
 * Created by demaksee on 6/2/15.
 */
public class YacoseExampleApplication extends Application{

	private static YacoseExampleApplication instance;
	private ServiceHelper serviceHelper;

	public static YacoseExampleApplication getInstance() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		instance = this;

		serviceHelper = new ServiceHelper(this, new Handler());

		Yacose.setLoggingEnabled(true);
	}

	public ServiceHelper getServiceHelper() {
		return serviceHelper;
	}
}
