package com.demaksee.yacose.example;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.demaksee.yacose.BaseServiceHelper;
import com.demaksee.yacose.example.service.ServiceHelper;
import com.demaksee.yacose.example.service.command.CounterCommand;
import com.demaksee.yacose.handler.AbstractCommand;
import com.demaksee.yacose.request.Request;
import com.demaksee.yacose.result.Result;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by demaksee on 17.01.16.
 */
public class SerialExampleActivity extends Activity {

    private static final String TAG = SerialExampleActivity.class.getSimpleName();

    private static final String IDS = "ids";

    private Random random = new Random(System.currentTimeMillis());

    private ServiceHelper serviceHelper;
    private ViewGroup container;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serial_example);

        serviceHelper = YacoseExampleApplication.getInstance().getServiceHelper();

        container = (ViewGroup) findViewById(R.id.container);

        if (savedInstanceState != null) {
            ArrayList<Integer> ids = savedInstanceState.getIntegerArrayList(IDS);
            for (Integer requestId : ids) {
                int max = savedInstanceState.getInt(Integer.toString(requestId));
                boolean single = savedInstanceState.getBoolean("single" + Integer.toString(requestId), false);
                addProgressBar(max, requestId, single);
            }
        }

        findViewById(R.id.buttonAddCommand).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int max = random.nextInt(40) + 11;
                int requestId = serviceHelper.startCounter(max, false, false);

                addProgressBar(max, requestId, false);
            }
        });

        findViewById(R.id.buttonAddSerialCommand).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int max = random.nextInt(20) + 11;
                int requestId = serviceHelper.startCounter(max, true, false);

                addProgressBar(max, requestId, true);
            }
        });

        findViewById(R.id.buttonAddSerialAndCancelPrior).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int max = random.nextInt(20) + 11;
                int requestId = serviceHelper.startCounter(max, true, true);

                addProgressBar(max, requestId, true);
            }
        });
    }

    private void addProgressBar(int max, final int requestId, final boolean serial) {
        View view = getLayoutInflater().inflate(R.layout.progress_bar_item, container, false);
        if (serial) {
            view.setBackgroundColor(Color.YELLOW);
        }

        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setIndeterminate(false);
        progressBar.setMax(max);

        view.findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceHelper.cancelRequest(requestId);
            }
        });

        container.addView(view);
        scrollDown();

        view.setTag(requestId);
    }

    private void scrollDown() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ScrollView scrollView = (ScrollView) container.getParent();
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        serviceHelper.addListener(serviceResultListener);
    }

    @Override
    protected void onPause() {
        super.onPause();

        serviceHelper.removeListener(serviceResultListener);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ArrayList<Integer> ids = new ArrayList<>(container.getChildCount());
        for (int i = 0; i < container.getChildCount(); i++) {
            View view = container.getChildAt(i);
            int requestId = (int) view.getTag();
            if (serviceHelper.isPending(requestId)) { // ignoring already finished tasks
                ids.add(requestId);
                ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
                outState.putInt(Integer.toString(requestId), progressBar.getMax());
                if (view.getBackground() instanceof ColorDrawable) {
                    outState.putBoolean("single" + Integer.toString(requestId), true);
                }
            }
        }
        outState.putIntegerArrayList(IDS, ids);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFinishing()) {
            for (int i = 0; i < container.getChildCount(); i++) {
                View view = container.getChildAt(i);
                int requestId = (int) view.getTag();
                serviceHelper.cancelRequest(requestId);
            }
        }
    }

    private BaseServiceHelper.OnServiceResultListener serviceResultListener = new BaseServiceHelper.OnServiceResultListener() {
        @Override
        public void onServiceResult(Request request, int resultCode, Result result) {
            if (request.forCommand(CounterCommand.class)) {
                View parent = container.findViewWithTag(request.requestId);
                ProgressBar progressBar = (ProgressBar) parent.findViewById(R.id.progressBar);
                TextView textView = (TextView) parent.findViewById(R.id.textViewStatus);
                if (resultCode == AbstractCommand.RESULT_PROGRESS) {
                    progressBar.setProgress(result.progress.currentProgress);
                    textView.setText("in progress");
                } else if (resultCode == AbstractCommand.RESULT_SUCCESS) {
                    progressBar.setProgress(100);
                    textView.setText("finished");
                    parent.findViewById(R.id.buttonCancel).setVisibility(View.INVISIBLE);
                } else if (resultCode == AbstractCommand.RESULT_CANCELED) {
                    textView.setText("canceled");
                    parent.findViewById(R.id.buttonCancel).setVisibility(View.INVISIBLE);
                } else if (resultCode == AbstractCommand.RESULT_FAILURE) {
                    textView.setText("failed");
                    parent.findViewById(R.id.buttonCancel).setVisibility(View.INVISIBLE);
                }
            }
        }
    };
}
