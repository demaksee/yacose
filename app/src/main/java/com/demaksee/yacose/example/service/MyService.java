package com.demaksee.yacose.example.service;

import com.demaksee.yacose.service.CommandExecutorService;

import java.util.concurrent.ExecutorService;

/**
 * Created by demaksee on 6/2/15.
 */
public class MyService extends CommandExecutorService {

    @Override
    protected ExecutorService onCreateExecutor(int shutdownDelay) {
        // you can create your executor here
        return super.onCreateExecutor(shutdownDelay);
    }

    @Override
    protected int getShutdownDelay() {
        // you can override default shutdown delay here
        return super.getShutdownDelay();
    }
}
