package com.demaksee.yacose.example.service.command;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;

import com.demaksee.yacose.exception.CanceledTaskException;
import com.demaksee.yacose.exception.FailedTaskException;
import com.demaksee.yacose.handler.AbstractCommand;

/**
 * Created by demaksee on 6/2/15.
 */
public class SumCommand extends AbstractCommand<SumCommand.SumData> {

	public SumCommand(Context context, Intent intent) {
		super(context, intent);
	}

	@Override
	public void run() {

		try {
			doSomething();

			postSuccess();
		} catch (CanceledTaskException e) {
			postCanceled();
		} catch (FailedTaskException e) {
			postFailure(e.getMessage());
		}
	}

	private void doSomething() throws FailedTaskException, CanceledTaskException {
		try {
			SystemClock.sleep(1000);
			postProgress(null, 33, 100);
			checkCanceled(); // check whether current task is canceled and throw exception if true

			// your actual work goes here

			SystemClock.sleep(2000);
			postProgress(null, 66, 100);
			checkCanceled();

			SystemClock.sleep(3000);
			postProgress(null, 100, 100);
			checkCanceled();

			SumData sumData = getClientData();

			sumData.result = sumData.a + sumData.b; // leave result in the same clientData object it will be sent back in result

		} catch (RuntimeException e){ // RuntimeException just for example
			throw new FailedTaskException(e.getMessage());
		}
	}

	/**
	 * Structure to hold parameters and results.
	 * You can use any plugin to generate parcelable boilerplate code:
	 * Android studio plugin: https://github.com/mcharmas/android-parcelable-intellij-plugin
	 * or something like: https://android-arsenal.com/details/1/1039
	 */
	public static class SumData implements Parcelable {
		public int a;

		public int b;

		public int result;

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeInt(this.a);
			dest.writeInt(this.b);
			dest.writeInt(this.result);
		}

		public SumData() {
		}

		private SumData(Parcel in) {
			this.a = in.readInt();
			this.b = in.readInt();
			this.result = in.readInt();
		}

		public static final Parcelable.Creator<SumData> CREATOR = new Parcelable.Creator<SumData>() {
			public SumData createFromParcel(Parcel source) {
				return new SumData(source);
			}

			public SumData[] newArray(int size) {
				return new SumData[size];
			}
		};
	}
}
