package com.demaksee.yacose.example.service;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.demaksee.yacose.BaseServiceHelper;
import com.demaksee.yacose.example.service.command.CounterCommand;
import com.demaksee.yacose.example.service.command.SumCommand;

/**
 * Created by demaksee on 6/2/15.
 */
public class ServiceHelper extends BaseServiceHelper {
	public ServiceHelper(Context context, Handler handler) {
		super(context, handler, MyService.class);
	}

	public int calculateSum(int a, int b) {
		final int requestId = createId();

		SumCommand.SumData sumData = new SumCommand.SumData();
		sumData.a = a;
		sumData.b = b;

		Intent intent = createIntent(SumCommand.class, requestId, sumData);

		return runRequest(requestId, intent);
	}

	public int startCounter(int max, boolean serial, boolean cancelPriorRequests) {
		final int requestId = createId();

		CounterCommand.Data data = new CounterCommand.Data();
		data.max = max;

		Intent intent = createIntent(CounterCommand.class, requestId, data);

		if (serial) {
			return runRequestInSeries(requestId, intent, cancelPriorRequests);
		}
		return runRequest(requestId, intent);
	}
}
